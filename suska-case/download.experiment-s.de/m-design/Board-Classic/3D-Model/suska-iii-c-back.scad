difference(){
    cube([238,1,32]); // Outer contour.
    // Holes:
    translate([238,1,0])rotate([90,0,0])mirror([1,0,0])linear_extrude(height=1,$fn=50)import("suska-iii-c-back.dxf");
};