// 0 = bottom half, 1 = top half, 2=uncut object, 3=cutter, 4=object+cutter
tophalf=0;       // [0,1,2,3,4]

// lift object which extends below bed
adjustheight=0;   // [300]

// where to split
splitheight=100;  // [300]

// input filename
infile="suska-iii-c-bottom.stl";

// flip bottom half upside down
flip_vertical=0;  // [0,1]

// convexity has to do with complexity of input shape; increase if preview ugly
convexity=10; // [0:50]

module theobject() {
   translate([0,0,adjustheight]) import(infile, convexity=convexity);
}

module thecutter() {
   translate([0,0,splitheight/2]) 
       cube([500,500, splitheight], center=true);
}

if(tophalf==1) {
   translate([0,0,-splitheight]) 
      difference() {
          theobject();
          thecutter();
      }
} else if(tophalf==0) {
      translate([0,0,flip_vertical*splitheight]) rotate([0,flip_vertical*180,0])  
        intersection() {
            theobject();
            thecutter();
        }
} else if(tophalf==2) {
    theobject();
} else if(tophalf==3) {
    thecutter();
} else {
    union() {
        theobject();
        thecutter();
    }
}