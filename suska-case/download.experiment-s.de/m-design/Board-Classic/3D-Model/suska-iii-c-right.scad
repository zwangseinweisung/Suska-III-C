difference(){
    translate([140,0,0])cube([1,140,32]); // Outer contour.
    // Holes:
    translate([140,0,0])rotate([90,0,90])linear_extrude(height=1.01,$fn=50)import("suska-iii-c-right.dxf");
};