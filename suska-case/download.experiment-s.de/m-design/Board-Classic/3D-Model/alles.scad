// Boden
difference(){
    linear_extrude(height=5,$fn=50)
    import("suska-iii-c-bottom1.dxf"); //Contour.
    
    translate([0,0,4])
    linear_extrude(height=2,$fn=100)
    import("suska-iii-c-bottom2.dxf"); //Milling.
};

// Front
#difference(){
    translate([7,6.5,4])
    cube([238,1,32]); // Outer contour.
    
    // Holes:
    translate([7,8,4])
    rotate([90,0,0])
    linear_extrude(height=2,$fn=100)
    import("suska-iii-c-front.dxf");
};

// rechte Seite
difference(){
    translate([244.5,6.5,4])
    cube([1,140,32]); // Outer contour.
    
    // Holes:
    translate([244.5,6.5,4])rotate([90,0,90])
    linear_extrude(height=2,$fn=100)
    import("suska-iii-c-right.dxf");
};


// linke seite
difference(){
    translate([6.5,6.5,4])
    cube([1,140,32]); // Outer contour.
    
    // Holes:
    translate([6.5,146.5,4])
    rotate([90,0,90])
    mirror([1,0,0])
    linear_extrude(height=2,$fn=100)
    import("suska-iii-c-left.dxf");
};

// Rückseite
difference(){
    
    translate([6.5,148.5,4])
    cube([238,1,32]); // Outer contour.
    
    // Holes:
    translate([244.5,149.5,4])rotate([90,0,0])
    mirror([1,0,0])
    linear_extrude(height=2,$fn=100)
    import("suska-iii-c-back.dxf");
};

// Deckel
difference(){
    translate([-10,-10,36])
    linear_extrude(height=1,$fn=100)

    import("suska-iii-c-top1.dxf"); //Contour.
    translate([10,10,44])
    linear_extrude(height=2,$fn=100)
    import("suska-iii-c-top2.dxf"); //Milling.
};
