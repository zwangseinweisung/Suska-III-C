difference(){
    cube([1,140,32]); // Outer contour.
    // Holes:
    translate([0,140,0])rotate([90,0,90])mirror([1,0,0])linear_extrude(height=1.01,$fn=50)import("suska-iii-c-left.dxf");
};